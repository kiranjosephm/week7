package week7;

import static org.junit.Assert.*;

import org.junit.Test;

public class circleTest {

		// 1. Create a Junit file  - DONE
		// 2. Write test case in Junit file 
		// 3. Run yout test case
		// 4. See what passed & failed
		
		
		
		// @Test -> 
		// Tells Junit to run the function as a test case
		// & report back if the test case passed or failed
		@Test
		public void testConstructor() {
			circle c = new circle(5);
			
			double expectedRadius = 5;
			double actualRadius = c.getRadius();
			
			//			assertEquals(x,y,0.01);
			// x = expected output
			// y = actual output
			// 0.01 = just use 0.01
			// OPTION 1: WRite it like this:
			// 		assertEquals(5, c.getRadius(), 0.01);	
			
			assertEquals(expectedRadius,actualRadius,0.01);
		}
		
		@Test
		public void testAreaFunction() 
		{
			circle c = new circle(5);
			
			double expectedArea = 78.5;
			double actualArea = c.getArea();
			
			assertEquals(expectedArea,actualArea,0.1);
		}
		
		
		@Test
		public void testCircumFunction() 
		{
			circle c = new circle(5);
			
			double expectedC = Math.PI * 2 * 5;
			double actualC = c.getCircumference();
			
			assertEquals(expectedC,actualC,0.1);
		}
		
		
		
		@Test
		public void testDiameterFunction() 
		{
			circle c = new circle(5);
			
			double expectedDiameter = 10;
			double actualDiameter = c.getDiameter();
			
			assertEquals(expectedDiameter,actualDiameter,0.1);
		}
		
}
