package week7;

public class main 
{

	public static void main(String[] args) 
	{
		// PROCESS FOR TESTING CODE
				// ----------------------------
				// 1. Read the requirements!
				// 2. Decide what to test!
						// R1:  Constructor sets radius
						// R2:  Areas calculates properly
						// R3:  Circumference calculates properly
						// R4:  Diameter calculates properly
				// 3. Make test cases
				//		// Test case = the thing you are trying to test
						// Working?
						// Expected Result  --> What SHOULD happen
						// Actual Result	--> What ACTUALLY happened
				
						// Test case:
						//		-- Working   (expected == actual)
						//		-- Not work	 (expected != actual)
				
				// 3.
				// 4.
				// 5. Debug
				// 6. 
		
		// R1: Constructor sets radius
		// Expected:  radius = 5
		// Actual:  c.getRadius()
		circle c = new circle(5);
				
		double expectedRadius = 5;
		double actualRadius = c.getRadius();
				
		if (expectedRadius == actualRadius) {
			System.out.println("R1: Working!");
			}
		else {
			System.out.println("R1: NOT Working!");
			System.out.println("Expected Result: " + expectedRadius);
			System.out.println("Actual Result: " + actualRadius);
			}
		
		// R2: Areas calculates properly
		// Assume radius = 5
		// Expected:  area = (radius x radius) x PI = 78.5
		// Actual:  c.getRadius()
				
		double expectedArea = 78.5;
		double actualArea = c.getArea();
				
		if (expectedArea == actualArea) {
			System.out.println("R2: Working!");
			}
		else {
			System.out.println("R2: NOT Working!");
			System.out.println("Expected Result: " + expectedArea);
			System.out.println("Actual Result: " + actualArea);
			}
		
		// R3: Circumference calculates properly
		// Assume radius = 5
		// Expected: circume = Math.PI * 2 * this.radius
		// Actual:  c.getRadius()
				
		double expectedC = Math.PI * 2 * 5;
		double actualC = c.getCircumference();
				
		if (expectedC == actualC) {
			System.out.println("R3: Working!");
			}
		else {
			System.out.println("R3: NOT Working!");
			System.out.println("Expected Result: " + expectedC);
			System.out.println("Actual Result: " + actualC);
			}
				
		// R4: Diameter calculates properly
		// Assume radius = 5
		// Expected: dia = Math.PI * 2 * this.radius
		// Actual:  c.getRadius()
				
		double expectedDiameter = 10;
		double actualDiameter = c.getDiameter();
			
		if (expectedDiameter == actualDiameter) {
			System.out.println("R4: Working!");
			}
		else {
			System.out.println("R4: NOT Working!");
			System.out.println("Expected Result: " + expectedDiameter);
			System.out.println("Actual Result: " + actualDiameter);
		}
	}

}
