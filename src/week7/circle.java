package week7;

public class circle 
{
	// PROPERTIES
		// --------------------
		private double radius = 5;
		
			
		// CONSTRUCTOR
		// --------------------
		public circle(double r) 
		{
			this.radius = r;
		}
		
		// CUSTOM METHODS
		// --------------------
		public double getArea() 
		{
			double area = Math.PI * this.radius * this.radius;
			return area;
		}
		public double getCircumference() 
		{
			double c = Math.PI * 2 * this.radius;
			return c;
		}
		public double getDiameter() 
		{
			double diameter = 2 * this.radius;
			return diameter;
		}
		
		
		// GETTERS AND SETTERS
		// --------------------
		public double getRadius() 
		{
			return radius;
		}


		public void setRadius(double radius) 
		{
			this.radius = radius;
		}
		
		
		
		
		
		
}
